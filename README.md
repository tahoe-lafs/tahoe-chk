# Tahoe-CHK

[![Most recent pipeline status badge](https://gitlab.com/tahoe-lafs/tahoe-chk/badges/main/pipeline.svg)](https://gitlab.com/tahoe-lafs/tahoe-chk/-/pipelines?scope=all&ref=main)

## What is it?

Tahoe-CHK is a Haskell implementation of the [Tahoe-LAFS](https://tahoe-lafs.org/) CHK crytographic protocol.
It aims for bit-for-bit compatibility with the original Python implementation.

It will not include an implementation of any network protocol for transferring CHK shares.
However, its APIs are intended to be easy to integrate with such an implementation.

### What is the current state?

* Convergent encryption is supported and compatible with Tahoe-LAFS.
* CHK encoding is implemented but some cases are unsupported:
  * It is not allowed that k == 1 or k == n.
* CHK decoding is implemented with the same limitations as for encoding.
  * The decoding process:
    * Authenticates the data being decoded using the capability.
    * Ensures the integrity of the data being decoded using the embedded hashes.

## Why does it exist?

A Haskell implementation can be used in places the original Python implementation cannot be
(for example, runtime environments where it is difficult to have a Python interpreter).
Additionally,
with the benefit of the experience gained from creating and maintaining the Python implementation,
a number of implementation decisions can be made differently to produce a more efficient, more flexible, simpler implementation and API.
Also,
the Python implementation claims no public library API for users outside of the Tahoe-LAFS project itself.
