# Changelog for tahoe-lafs-immutable-uploader

## 0.2.0.0 (2023-10-02)

* `decode` now checks the fingerprint in the read capability against the URI
  extension block in shares being decoded and signals an error instead of
  performing decoding if there are not enough shares where they match. (#18)

* `decode` now checks the consistency of the "share hash tree" information in
  each share and signals an error instead of performing decoding if there are
  not enough shares where it is consistent. (#19)

* `decode` now checks the consistency of each share's "blocks" and signals an
  error instead of performing decoding if there are not enough consistent
  blocks to complete decoding. (#20)

* `decode` now checks the consistency of the ciphertext "segments" and signals
  an error instead of completing decoding if the segment hashes do not match
  the expected values. (#21)

* Many changes to data types to reflect more of the scheme at the type level.
  Most of these changes are to (exposed) implementation details rather than
  the primary high-level interface and should not impact most applications.

* The `cereal`, `cipher-aes128`, `crypto-api`, `tagged`, and `monad-loop`
  direct dependencies have been dropped.  `cryptonite` (already a dependency)
  is now used for AES128 operations. (!54)

## 0.1.0.2

* `taggedPairHash` now respects the size parameter passed to it.
* The CHK capability parsers now signal error on overflow for the `n`, `k`, and `size` parameters.
* The UEB parser now signals error on overflow for `num_segments`,
  `needed_shares`, `total_shares`, and `n` and `k` in the codec parameter
  fields.

## 0.1.0.1

* Switch from `base64` to `base64-bytestring` to avoid an encoding bug on ARM
  with GHC 8.6.5.

## 0.1.0.0

* Initial release.
* Support for encoding and decoding data using Tahoe-LAFS' CHK protocol.
